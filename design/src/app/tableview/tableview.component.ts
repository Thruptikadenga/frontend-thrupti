import { Component, OnInit } from '@angular/core';
import {DialogComponent} from './Dialogcomponent/Dialogcomponent.component'

import { MatDialog } from '@angular/material';
// import { ModalService } from '../services/ModalService.service';
@Component({
  selector: 'app-tableview',
  templateUrl: './tableview.component.html',
  styleUrls: ['./tableview.component.css']
})
export class TableviewComponent implements OnInit {

  headerData = ["Avatar", "Name", "Email", "Experience"];

  data = [
    {
      Name: "Test",
      Email: "test@gmail.com",
      Experience: 2
    },
    {
      Name: "Data2",
      Email: "data2@gmail.com",
      Experience: 1
    }
  ]

  constructor(
    public dialog: MatDialog
    // private modalService: ModalService
    ) { }

  ngOnInit(): void {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      // data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

//   openModal(id: string) {
//     this.modalService.open(id);
// }



}
