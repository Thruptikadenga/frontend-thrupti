import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { TableviewComponent } from './tableview/tableview.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {path: '', component: TableviewComponent}
    // {path:'/ed', component: DialogBoxComponent
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
